# app_user_preferences 

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Preview

<img src="https://vsotelo-bucket-3.s3.us-west-004.backblazeb2.com/WhatsApp+Image+2022-03-31+at+9.05.37+AM+(2).jpeg" alt="Preview" />

<img src="https://vsotelo-bucket-3.s3.us-west-004.backblazeb2.com/WhatsApp+Image+2022-03-31+at+9.05.37+AM.jpeg" alt="Preview" />

<img src="https://vsotelo-bucket-3.s3.us-west-004.backblazeb2.com/WhatsApp+Image+2022-03-31+at+9.05.37+AM+(1).jpeg" alt="Preview" />